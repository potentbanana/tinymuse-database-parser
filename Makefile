build: deps
	env CGO_ENABLED=0 GOOS=windows GOARCH=386 go build -a -ldflags '-extldflags "-static"' -o bin/tmdbp.exe main.go
	env CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' -o bin/tmdbp main.go

test:
	mkdir -p coverage/
	go test ./... -v -coverprofile coverage/coverage.cov

fmt: ## Format
	go fmt ./...

deps: ## Get the dependencies
	@go get -v -d ./...
	@go get -u golang.org/x/lint/golint

all: build test fmt

