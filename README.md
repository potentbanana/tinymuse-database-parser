# TinyMUSE Database Parser

#### What is it?
This is a Golang-based TinyMUSE Database parser for extracting objects out of a TinyMUSE mdb. 

#### Command line arguments

##### database
> ./tmdbp --database /home/wm/muse/db/mdb


#### Internal Commands

##### Help Screen for TinyMUSE Database Parser v0.1

###### exit *or* quit			
Leaves the parser

###### help 			
Displays the help screen

###### examine `#REF`		
Show the object for that `#REF`

###### flags `#REF`		
Show objects with the flag (use int, not 0x#)

###### owned `#REF` 		
Show objects owned by `#REF`

###### search `text`		
Will search NAME for a particular user. Does *NOT* do a fulltext search across all attributes.
It returns the object # and Names that match.

#### Use these commands to save a new database, starting with new...

###### new			
Create a new database to move objects to

###### putall `#REF`		
Puts object and all its owned objects into the database; if the object does not own objects, it will be added by itself.

###### only `#REF`		
Puts only the object `#REF` into the new database

###### save `filename`
Saves the new database to an MDB file of `filename`.

