package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"strings"
)

const (
	MaximumNumberOfDatabases       = 10
	MaximumNumberOfDatabaseObjects = 100000
)

var (
	dbs       map[string]Database
	total_dbs int
)

type (
	Attribute struct {
		Ref   int64  `json:"id"`
		Type  int64  `json:"type"`
		Value string `json:"value"`
	}

	AttributeDefinition struct {
		Flags int64  `json:"flags"`
		Ref   int64  `json:"id"`
		Name  string `json:"name"`
	}

	Object struct {
		Ref                  int64                 `json:"id"`
		Name                 string                `json:"name"`
		Location             int64                 `json:"location"`
		Zone                 int64                 `json:"zone"`
		Contents             int64                 `json:"contents"`
		Exits                int64                 `json:"exits"`
		Link                 int64                 `json:"link"`
		Next                 int64                 `json:"next"`
		Owner                int64                 `json:"owner"`
		Flags                int64                 `json:"flags"`
		CreateTime           int64                 `json:"create_time"`
		ModifiedTime         int64                 `json:"modified_time"`
		Powers               string                `json:"powers"`
		Attributes           []Attribute           `json:"attributes"`
		Parents              []int64               `json:"parents"`
		Children             []int64               `json:"children"`
		AttributeDefinitions []AttributeDefinition `json:"attribute_definitions"`
	}

	Database struct {
		Rows    map[int64]Object
		Version int64
		TopRef  int64
	}
)

func init() {
	total_dbs = 0
	dbs = make(map[string]Database)
}

func scanForString(scanner *bufio.Scanner) string {
	scanner.Scan()
	return scanner.Text()
}

func scanForInt(scanner *bufio.Scanner) int64 {
	scanner.Scan()
	x := scanner.Text()
	s, err := strconv.ParseInt(x, 10, 64)
	if err != nil {
		fmt.Println("Couldn't determine location; returning 0", err)
	}
	return s
}

func getAttributeDefinitions(scanner *bufio.Scanner) []AttributeDefinition {
	var atrs []AttributeDefinition
	for scanner.Scan() {
		x := scanner.Text()
		switch x[0] {
		case '\\':
			return atrs
		case '/': // Found an attribute definition
			flags, _ := strconv.ParseInt(x[1:], 10, 64)
			atrs = append(atrs, AttributeDefinition{
				Flags: flags,
				Ref:   scanForInt(scanner),
				Name:  scanForString(scanner),
			})
		}
	}
	return atrs
}

func writeAttrdefs(f *os.File, atrdefs []AttributeDefinition) {
	if len(atrdefs) <= 0 {
		return
	}
	for x := range atrdefs {
		_, _ = f.WriteString(fmt.Sprintf("/%d\n", atrdefs[x].Flags))
		_, _ = f.WriteString(fmt.Sprintf("%d\n", atrdefs[x].Ref))
		_, _ = f.WriteString(fmt.Sprintf("%s\n", atrdefs[x].Name))
	}
}

func findDefaultOwner(db map[int64]Object) int64 {
	for x := range db {
		if db[x].Flags&0x8 == 0x8 {
			return x
		}
	}
	return -1
}

func getAttributes(scanner *bufio.Scanner) []Attribute {
	fmt.Println("Scanning for attributes...")
	var atrs []Attribute
Loop:
	for scanner.Scan() {
		x := scanner.Text()
		switch x[0] {
		case '>':
			ref, _ := strconv.ParseInt(x[1:], 10, 64)
			atrs = append(atrs, Attribute{
				Ref:   ref,
				Type:  scanForInt(scanner),
				Value: scanForString(scanner),
			})
		case '<':
			break Loop
		default:
			fmt.Printf("Illegal attribute; unable to determine this character: %c\n", x[0])
		}
	}
	return atrs
}

func writeAttributes(f *os.File, atrs []Attribute) {
	if len(atrs) > 0 {
		for a := range atrs {
			_, _ = f.WriteString(fmt.Sprintf(">%d\n", atrs[a].Ref))
			_, _ = f.WriteString(fmt.Sprintf("%d\n", atrs[a].Type))
			_, _ = f.WriteString(fmt.Sprintf("%s\n", atrs[a].Value))
		}
	}
	_, _ = f.WriteString("<\n")
}

func getList(scanner *bufio.Scanner) []int64 {
	scanner.Scan()
	x := scanner.Text()
	y, _ := strconv.ParseInt(x, 10, 32)
	if y == 0 {
		return []int64{}
	}
	list := []int64{}
	for cnt := int64(1); cnt <= y; cnt++ {
		scanner.Scan()
		v := scanner.Text()
		i, _ := strconv.ParseInt(v, 10, 64)
		list = append(list, i)
	}
	return list
}

func writeList(f *os.File, items []int64) {
	if len(items) == 0 {
		_, _ = f.WriteString("0\n")
		return
	}
	_, _ = f.WriteString(fmt.Sprintf("%d\n", len(items)))
	for x := range items {
		_, _ = f.WriteString(fmt.Sprintf("%d\n", items[x]))
	}
}

func writeJson(o Object) {
	f, err := json.Marshal(o)
	if err != nil {
		fmt.Println("Can't show this object", err)
		return
	}
	fmt.Println(string(f))
}

func showObject(obj Object) {
	objStr := `
DBREF: #%d
Name: %s
Location: #%d
Zone: #%d
Contents: %d
Exits: %d
Link: #%d
Next: %d
Owner: #%d
Flags: %d
CreatedTime: %d
ModifiedTime: %d
Powers: %s
`
	objStr = fmt.Sprintf(objStr, obj.Ref, obj.Name, obj.Location, obj.Zone, obj.Contents, obj.Exits, obj.Link, obj.Next, obj.Owner, obj.Flags, obj.CreateTime, obj.ModifiedTime, obj.Powers)
	fmt.Print(objStr)
	fmt.Println("Attributes:\n---")
	if len(obj.Attributes) == 0 {
		fmt.Println("No attributes found.")
	} else {
		for _, atr := range obj.Attributes {
			fmt.Printf("\tRef: %d\n\tType: %d\n\tValue:%s\n", atr.Ref, atr.Type, atr.Value)
		}
	}
	fmt.Println("Children:")
	if len(obj.Children) > 0 {
		x := []string{}
		for i := range obj.Children {
			x = append(x, fmt.Sprintf("#%d", obj.Children[i]))
		}
		fmt.Println(strings.Join(x, ","))
	} else {
		fmt.Println("No children found")
	}
	fmt.Println("Parents:")
	if len(obj.Parents) > 0 {
		x := []string{}
		for i := range obj.Parents {
			x = append(x, fmt.Sprintf("#%d", obj.Parents[i]))
		}
		fmt.Println(strings.Join(x, ","))
	} else {
		fmt.Println("No parents found.")
	}
	fmt.Println("Attribute Definitions:\n---")
	if len(obj.AttributeDefinitions) == 0 {
		fmt.Println("No attribute definitions found.")
	} else {
		for _, def := range obj.AttributeDefinitions {
			fmt.Printf("\tFlags: %d\n\tRef: %d\n\tName: %s\n", def.Flags, def.Ref, def.Name)
		}
	}
	fmt.Println("--- END OF OBJECT ---")
}

func ParseDatabase(dbName string, filename string) (string, error) {
	f, err := os.Open(filename)
	defer f.Close()
	if err != nil {
		fmt.Printf("Unable to find the file you requested: %s; error=%s\n", filename, err)
	}
	db := Database{}
	db.Rows = make(map[int64]Object)
	scanner := bufio.NewScanner(f)
Start:
	for scanner.Scan() {
		x := scanner.Text()
		switch x[0] {
		case '@':
			fmt.Println("Database version is", x[1:])
			v, err := strconv.ParseInt(x[1:], 10, 64)
			if err != nil {
				panic(fmt.Sprintf("Cannot find version for database %s", err))
			}
			db.Version = v
		case '~':
			fmt.Printf("There are %s objects in the database\n", x[1:])
			cnt, err := strconv.ParseInt(x[1:], 10, 64)
			if err != nil {
				panic(fmt.Sprintf("Cannot determine how many objects in mdb: %s", err))
			}
			db.TopRef = cnt
			fmt.Printf("Made the db: %#v\n", db)
		case '&':
			if db.TopRef <= 0 {
				panic("unable to continue; haven't determined size of database")
			}
			fmt.Println("Object # (DBREF) is: ", x[1:])
			ref, err := strconv.ParseInt(x[1:], 10, 64)
			if err != nil {
				fmt.Println("Cannot parse this object's dbref ", err)
			}
			obj := Object{
				Ref: ref,
			}
			// Get Name
			obj.Name = scanForString(scanner)
			obj.Location = scanForInt(scanner)
			obj.Zone = scanForInt(scanner)
			obj.Contents = scanForInt(scanner)
			obj.Exits = scanForInt(scanner)
			obj.Link = scanForInt(scanner)
			obj.Next = scanForInt(scanner)
			obj.Owner = scanForInt(scanner)
			obj.Flags = scanForInt(scanner)
			if db.Version >= 10 {
				obj.CreateTime = scanForInt(scanner)
				obj.ModifiedTime = scanForInt(scanner)
			}
			if obj.Flags&int64(0x8) == 0x8 {
				obj.Powers = scanForString(scanner)
			} else {
				obj.Powers = ""
			}
			fmt.Println("Version", db.Version)
			obj.Attributes = getAttributes(scanner)
			obj.Parents = getList(scanner)
			obj.Children = getList(scanner)
			obj.AttributeDefinitions = getAttributeDefinitions(scanner)
			db.Rows[ref] = obj
		case '*':
			if strings.Compare(x, "***END OF DUMP***") == 0 {
				fmt.Println("All done scanning database; skipping +mail/end of database.")
				break Start
			}
		default:
			fmt.Println("Unknown info:", x)
		}
	}
	if err := scanner.Err(); err != nil {
		panic(fmt.Sprintf("Unable to scan file: %s", err))
	}

	dbs[dbName] = db
	return dbName, nil
}

func createNewDatabase() Database {
	db := Database{}
	db.Version = 10
	db.TopRef = 0
	db.Rows = make(map[int64]Object)
	db.Rows[0] = Object{}
	return db
}

func main() {
	var dbDefault string
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print("> ")
		text, _ := reader.ReadString('\n')
		text = strings.Replace(text, "\r\n", "", -1)
		text = strings.Replace(text, "\n", "", -1)
		splitStr := strings.Split(text, " ")
		var cmd string
		var ref int64
		numParts := len(splitStr)
		cmd = splitStr[0]
		if numParts > 1 {
			for i := 1; i < len(splitStr); i++ {
				if strings.TrimSpace(splitStr[i]) == "" {
					continue
				}
				if splitStr[i][0] == '#' {
					ref, _ = strconv.ParseInt(splitStr[i][1:], 10, 64)
				}
			}
		}
		if strings.Compare("exit", text) == 0 || strings.Compare("quit", text) == 0 {
			fmt.Println("Thank you for using the TinyMUSE Database Parser")
			break
		} else if strings.HasPrefix("use", cmd) {
			if numParts < 2 {
				fmt.Println("Unknown database.")
				continue
			}
			dbn := splitStr[1]
			if _, ok := dbs[dbn]; !ok {
				fmt.Println("Unknown database ", dbn)
			}
			dbDefault = dbn
		} else if strings.HasPrefix("set", cmd) {
			if numParts < 3 {
				fmt.Println("Unknown setting.")
				continue
			}
			setting := splitStr[1]
			if setting == "db" || setting == "default" {
				dbDefault = splitStr[2]
			}
		} else if strings.HasPrefix("help", cmd) {
			writeHelp()
		} else if strings.HasPrefix("examine", cmd) {
			if item, ok := dbs[dbDefault].Rows[ref]; ok {
				showObject(item)
			}
		} else if strings.HasPrefix("owned", cmd) {
			for _, item := range dbs[dbDefault].Rows {
				if item.Owner == ref {
					writeJson(item)
				}
			}
		} else if strings.HasPrefix("list", cmd) {
			if numParts < 2 {
				for k, _ := range dbs {
					fmt.Println("Database ", k)
				}
				continue
			}
			dbn := splitStr[1]
			fmt.Printf("[%s]\n", dbn)
			if strings.TrimSpace(dbn) == "" {
				continue
			}
			for _, item := range dbs[dbn].Rows {
				fmt.Printf("%s (#%d)\n", item.Name, item.Ref)
			}
		} else if strings.HasPrefix("flags", cmd) {
			for _, item := range dbs[dbDefault].Rows {
				if item.Flags&ref == ref {
					writeJson(item)
				}
			}
			//} else if strings.HasPrefix("new", cmd) {
			//	fmt.Println("Creating a new database to filter objects into...")
			//	if int64(len(newDB)) < db_top {
			//		newDB = make(map[int64]Object, db_top)
			//		new_dbtop = db_top
			//	}
		} else if strings.HasPrefix("open", cmd) {
			if total_dbs >= MaximumNumberOfDatabases {
				fmt.Println("You cannot load anymore databases; maximum = ", MaximumNumberOfDatabases)
				continue
			}
			if numParts < 3 {
				fmt.Println("We cannot find that database to load.")
				continue
			}
			dbn := splitStr[1]
			fname := splitStr[2]
			ParseDatabase(dbn, fname)
			dbDefault = dbn
			total_dbs++
		} else if strings.HasPrefix("create", cmd) {
			if numParts < 2 {
				fmt.Println("Unable to create database; no name given.")
				continue
			}
			dbn := splitStr[1]
			db := createNewDatabase()
			dbs[dbn] = db
		} else if strings.HasPrefix("merge", cmd) {
			if numParts < 3 {
				fmt.Println("Not enough information on databases.")
				continue
			}
			source := splitStr[1]
			dest := splitStr[2]
			if _, ok := dbs[source]; !ok {
				fmt.Println("Cannot find source database", source)
				continue
			}
			if _, ok := dbs[dest]; !ok {
				fmt.Println("Cannot find destination database", dest)
				continue
			}
			db := dbs[source]
			for k, item := range db.Rows {
				dbs[dest].Rows[k] = item
			}
			fmt.Printf("Database %s copied to %s\n", source, dest)
		} else if strings.HasPrefix("copy", cmd) {
			if numParts < 3 {
				fmt.Println("Unable to copy unknown object")
				continue
			}
			ref, _ = strconv.ParseInt(splitStr[1][1:], 10, 64)
			fmt.Println("ref", ref)
			name := splitStr[2]
			var newRef int64
			if numParts == 4 {
				newRef, _ = strconv.ParseInt(splitStr[3][1:], 10, 64)
			} else {
				newRef = ref
			}
			obj := dbs[dbDefault].Rows[ref]
			if ref != newRef {
				obj.Ref = newRef
				obj.Owner = newRef
			}
			if _, ok := dbs[name]; !ok {
				fmt.Println("Unable to copy object to missing database", name)
				continue
			}
			dbs[name].Rows[newRef] = obj
			fmt.Printf("Copied #%d from %s to #%d on %s\n", ref, dbDefault, newRef, name)
			//		} else if strings.HasPrefix("putall", cmd) {
			//			for k, _ := range db {
			//				if db[k].Owner == ref {
			//					newDB[k] = db[k]
			//				}
			//			}
			//			fmt.Println("Objects added to new database")
			//		} else if strings.HasPrefix("only", cmd) {
			//			if _, ok := db[ref]; ok {
			//				newDB[ref] = db[ref]
			//			}
			//			fmt.Println("Single object added to new database")
			//		} else if (strings.HasPrefix("search", cmd)) {
			//			fmt.Println("Search Results:\n---")
			//			for _, v := range db {
			//				if strings.Contains(strings.ToLower(v.Name), strings.ToLower(rem)) {
			//					fmt.Printf("%s (#%d)\n", v.Name, v.Ref)
			//				}
			//			}
			} else if strings.HasPrefix("save", cmd) {
				if numParts < 3 {
					fmt.Println("Missing filename and database")
					continue
				}
				name := splitStr[1]
				filename := splitStr[2]
				newF, err := os.Create(filename)
				if err != nil {
					fmt.Println("unable to open new database filename", err)
				}
				// Before we save, in order to properly address the database, we need to look up a player
				// and a location to store nil objects in.
				if _, ok := dbs[name]; !ok {
					fmt.Println("Unable to find that database", name)
					continue
				}
				//dO := findDefaultOwner(dbs[name].Rows)
				_, _ = newF.WriteString(fmt.Sprintf("@%d\n", dbs[name].Version))
				_, _ = newF.WriteString(fmt.Sprintf("~%d\n", len(dbs[name].Rows)))
				top := int64(len(dbs[name].Rows))
				for k := int64(0); k < top; k++ {
					item := dbs[name].Rows[k]
//				for k := int64(0); k < dbs[name].TopRef; k++ {
//					if _, ok := newDB[k]; !ok {
//						// Write a fake object
//						fakeObjStr := `&%d
//Filler Object From TMDBP
//%d
//0
//1
//-1
//0
//-1
//%d
//135307265
//0
//0
//<
//0
//0
//\
//`
//						_, _ = newF.WriteString(fmt.Sprintf(fakeObjStr, k, k, dO))
//						continue
//					}
//					item := newDB[k]
					itemStr := `&%d
%s
%d
%d
%d
%d
%d
%d
%d
%d
%d
%d
`
					_, _ = newF.WriteString(fmt.Sprintf(itemStr,
						item.Ref,
						item.Name,
						item.Location,
						item.Zone,
						item.Contents,
						item.Exits,
						item.Link,
						item.Next,
						item.Owner,
						item.Flags,
						item.CreateTime,
						item.ModifiedTime))
					if item.Flags&int64(0x8) == 0x8 {
						_, _ = newF.WriteString(fmt.Sprintf("%s\n", item.Powers))
					}
					writeAttributes(newF, item.Attributes)
					writeList(newF, item.Parents)
					writeList(newF, item.Children)
					writeAttrdefs(newF, item.AttributeDefinitions)
					_, _ = newF.WriteString("\\\n")
				}
				_, _ = newF.WriteString("***END OF DUMP***\n")
				fmt.Println("Save of Database Complete:", name)
		}
	}
}

func writeHelp() {
	helpStr := `
Help Screen for TinyMUSE Database Parser v0.1
---
exit 					Good Bye
help 					The HELP screen
set default <name>		Set the current database to <name>
open <name> <filename>	Loads a database into memory
examine #<ref>			Show the object for that ref
flags #<ref> 			Show objects with the flag (use int, not 0x???)
owned #<ref> 			Show objects owned by <ref>
search <text>			Will search NAME for a particular user. Does *NOT* do a fulltext search across all attributes.
						It returns the object # and Names that match.
--- 
Use these commands to save a new database, starting with new...

copy #<ref> <name> #<newref>  Copies the object to the new database.
create <name> 		Creates a new database
putall #<ref>		Puts object and all its owned objects into the database; if the object does not own objects, it will be added by itself.
only #<ref>			Puts only the object <ref> into the new database
save <filename>		Saves the new database to an MDB file of <filename>.
---
`
	fmt.Println(helpStr)
}